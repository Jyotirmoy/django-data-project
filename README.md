# Django Data Project

Create and activate virtualenv
------
1. `mkdir venv`
2. `cd venv`
3. `virtualenv -p python3 django-data-project`
4. `source bin/activate`

Install requirements
------
1. `mkdir djangodataproject`
2. `cd djangodataproject`
3. Clone the repository into the directory.
4. Activate virtual environment.
5. `pip install -r requirements.txt`

Set up database
------
1. Create a user in MySQL with `CREATE TABLE`, `DROP TABLE`, `SELECT` permissions.
2. Under `data_project` directory create a new file, `user_env_vars.py`.
3. Create constant variables `DBNAME`, `DBUSER`, `DBPASS` and pass them values that adhere to the database name, username and password for your MySQL database.

Create database tables
------
1. `python manage.py import_from_csv`

Run server
------
1. `python manage.py runserver`
2. Open web browser and visit http://127.0.0.1:8000
