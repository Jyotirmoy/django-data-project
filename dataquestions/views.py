from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.shortcuts import render
from django.views.decorators.cache import cache_page

from .question_id_decision_map import DECISION_MAP


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@cache_page(CACHE_TTL)
def index(request):
    return render(request, 'dataquestions/index.html')


@cache_page(CACHE_TTL)
def plot_data(request, question_id):
    execute_query = DECISION_MAP[question_id]
    query_set = execute_query()
    return render(request, 'dataquestions/plot_data.html', {'query_set': query_set,
                                                            'question_id': question_id, })
