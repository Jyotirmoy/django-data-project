from dataquestions.models import Matches, Deliveries

from django.core.management.base import BaseCommand
from django.db import transaction

import csv


matches_csv_path = 'dataquestions/resources/matches.csv'
deliveries_csv_path = 'dataquestions/resources/deliveries.csv'


def import_matches():
    with open(matches_csv_path, 'r') as matches_FH:
        matches_reader = csv.DictReader(matches_FH)
        for match in matches_reader:
            # Create an object of Matches model and save to database
            Matches(
                id=match['id'],
                season=match['season'],
                city=match['city'],
                date=match['date'],
                team1=match['team1'],
                team2=match['team2'],
                toss_winner=match['toss_winner'],
                toss_decision=match['toss_decision'],
                result=match['result'],
                dl_applied=match['dl_applied'],
                winner=match['winner'],
                win_by_runs=match['win_by_runs'],
                win_by_wickets=match['win_by_wickets'],
                player_of_match=match['player_of_match'],
                venue=match['venue'],
                umpire1=match['umpire1'],
                umpire2=match['umpire2'],
                umpire3=match['umpire3'],
            ).save()


def import_deliveries():
    with open(deliveries_csv_path, 'r') as deliveries_FH:
        deliveries_reader = csv.DictReader(deliveries_FH)
        for delivery in deliveries_reader:
            Deliveries(
                match=Matches.objects.get(id=delivery['match_id']),
                inning=delivery['inning'],
                batting_team=delivery['batting_team'],
                bowling_team=delivery['bowling_team'],
                over=delivery['over'],
                ball=delivery['ball'],
                batsman=delivery['batsman'],
                non_striker=delivery['non_striker'],
                bowler=delivery['bowler'],
                is_super_over=delivery['is_super_over'],
                wide_runs=delivery['wide_runs'],
                bye_runs=delivery['bye_runs'],
                legbye_runs=delivery['legbye_runs'],
                noball_runs=delivery['noball_runs'],
                penalty_runs=delivery['penalty_runs'],
                batsman_runs=delivery['batsman_runs'],
                extra_runs=delivery['extra_runs'],
                total_runs=delivery['total_runs'],
                player_dismissed=delivery['player_dismissed'],
                dismissal_kind=delivery['dismissal_kind'],
                fielder=delivery['fielder'],
            ).save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write('Importing matches.csv to database...')
        with transaction.atomic():
            import_matches()
        self.stdout.write('Imported matches.csv to database')
        self.stdout.write('Importing deliveries.csv to database...')
        with transaction.atomic():
            import_deliveries()
        self.stdout.write('Imported deliveries.csv to database')
