from django.urls import path

from . import views

app_name = 'dataquestions'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:question_id>/', views.plot_data, name='plot_data'),
]