from django.db.models import Avg, Count, Sum

from .models import Matches, Deliveries


def question_1():
    """Django ORM query for question 1"""
    return Matches.objects.all().values('season').annotate(total=Count('season')).order_by('season')


def question_2():
    """Django ORM query for question 2"""
    matches_won_by_teams_per_season = Matches.objects.values("season", "winner").exclude(
        winner__isnull=True).exclude(winner='').annotate(wins=Count('winner'))
    matches_per_team_per_season = {}
    for team in matches_won_by_teams_per_season:
        if team['winner'] not in matches_per_team_per_season.keys():
            matches_per_team_per_season[team['winner']] = {
                '2008': 0, '2009': 0, '2010': 0, '2011': 0, '2012': 0, '2013': 0, '2014': 0, '2015': 0, '2016': 0, '2017': 0}
        matches_per_team_per_season[team['winner']
                                    ][team['season']] = team['wins']
    return matches_per_team_per_season


def question_3():
    """Django ORM query for question 3"""
    matches_set = Matches.objects.only('season', 'id').filter(season=2016)
    return Deliveries.objects.only('match_id', 'bowling_team', 'extra_runs').filter(
        match_id__in=matches_set).only('bowling_team').values("bowling_team").annotate(runs=Sum("extra_runs"))


def question_4():
    """Django ORM query for question 4"""
    matches_in_2015 = Matches.objects.only(
        "season", "id").filter(season=2015)
    return Deliveries.objects.filter(match_id__in=matches_in_2015).values(
        "bowler").annotate(avg=Avg("total_runs") * 6).order_by('avg')[:8]


DECISION_MAP = {
    1: question_1,
    2: question_2,
    3: question_3,
    4: question_4,
}
